unit NoiseUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Edit, FMX.StdCtrls, FMX.Controls.Presentation;

type
  TNoiseForm = class(TForm)
    PanelMenu: TPanel;
    PanelButton: TPanel;
    PanelImage: TPanel;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    TrackBar: TTrackBar;
    Label1: TLabel;
    Edit: TEdit;
    ImageNoise: TImage;
    procedure FormCreate(Sender: TObject);
    procedure TrackBarChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NoiseForm: TNoiseForm;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

{procedure AddNoize(Bitmap:TBitmap; Amount:integer; Mono:boolean);
function BLimit(B:integer):byte;
begin
  if B<0 then
    Result:=0
  else if B>255 then
    Result:=255
  else Result:=B;
end;
type TRGB=record
  B,G,R:byte;
end;
  PRGB=^TRGB;
var
  x,y,i,a:integer;
  Dest:pRGB;
begin
  //Bitmap.PixelFormat:=;
  Randomize;
  i:=Amount shr 1;
  if mono then
    for y:=0 to Bitmap.Height-1 do begin
      Dest:=Bitmap.Unmap(y);
        for x:=0 to Bitmap.Width-1 do begin
          a:=random(Amount)-i;
          with Dest^ do begin
            r:=BLimit(r+a);
            g:=BLimit(g+a);
            b:=BLimit(b+a);
          end;
        Inc(Dest);
        end;
  end else
    for y:=0 to Bitmap.Height-1 do begin
      Dest:=Bitmap.Map[y];
      for x:=0 to Bitmap.Width-1 do begin
        with Dest^ do begin
          r:=BLimit(r+random(Amount)-i);
          g:=BLimit(g+random(Amount)-i);
          b:=BLimit(b+random(Amount)-i);
        end;
      Inc(Dest);
      end;
    end;
end;    }

procedure TNoiseForm.FormCreate(Sender: TObject);
begin
  //TrackBar.Position:=0;
end;

procedure TNoiseForm.TrackBarChange(Sender: TObject);
begin
  //Edit.Text:=IntToStr(TrackBar.Position);
end;

end.
