unit EffectUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.ImageList, FMX.ImgList, FMX.ListBox, FMX.Layouts, NoiseUnit, FMX.Filter.Effects,
  FireDAC.Stan.Def, FireDAC.Phys.SQLiteWrapper, FireDAC.Stan.Intf, FireDAC.Phys,
  FireDAC.Phys.SQLite, ContrastUnit, ConfigUnit;

type
  TEffectForm = class(TForm)
    ListBox1: TListBox;
    NoiseItem: TListBoxItem;
    EffectImageList: TImageList;
    MonohromeItem: TListBoxItem;
    InvertItem: TListBoxItem;
    FDSQLiteFunction1: TFDSQLiteFunction;
    SepiaItem: TListBoxItem;
    ContrastItem: TListBoxItem;
    BlurItem: TListBoxItem;
    ShadowItem: TListBoxItem;
    procedure NoiseItemClick(Sender: TObject);
    procedure SepiaItemClick(Sender: TObject);
    procedure ContrastItemClick(Sender: TObject);
    procedure InvertItemClick(Sender: TObject);
    procedure MonohromeItemClick(Sender: TObject);
    procedure BlurItemClick(Sender: TObject);
    procedure ShadowItemClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EffectForm: TEffectForm;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

Uses
  MainUnit;

function AddEffects(const Effect: TImageFXEffect):TImageFXEffect;
var i:integer;
begin
if FormConfig.SwitchEffect.IsChecked = false then
  begin
    for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
      if MainForm.ImagePanel.Children[i] is TImageFXEffect then
      begin
        MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
        break;
      end;
      MainForm.ImagePanel.AddObject(Effect);
  end
  else begin
    EffectsList.Add(Effect);
    ApplayEffects(MainForm.ImagePanel.Bitmap, EffectsList);
  end;
  EffectForm.Close;
end;

procedure TEffectForm.BlurItemClick(Sender: TObject);
var i:integer;
begin
  if FormConfig.SwitchEffect.IsChecked = false then
  begin
    for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
      if MainForm.ImagePanel.Children[i] is TImageFXEffect then
      begin
        MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
        break;
      end;
      MainForm.ImagePanel.AddObject(MainForm.BlurEffect1);
  end
  else begin
    //EffectsList.Add(MainForm.BlurEffect1);
    ApplayEffects(MainForm.ImagePanel.Bitmap, EffectsList);
  end;
  EffectForm.Close;
end;

procedure TEffectForm.ContrastItemClick(Sender: TObject);
begin
  ContrastForm.Show;
  ContrastForm.Image1.Bitmap.Assign(MainForm.ImagePanel.Bitmap);
end;

procedure TEffectForm.InvertItemClick(Sender: TObject);
var i:integer;
begin
 { if FormConfig.SwitchEffect.IsChecked = false then
  begin
    for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
      if MainForm.ImagePanel.Children[i] is TImageFXEffect then
      begin
        MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
        break;
      end;
      MainForm.ImagePanel.AddObject(MainForm.InvertEffect1);
  end
  else begin
    EffectsList.Add(MainForm.InvertEffect1);
    ApplayEffects(MainForm.ImagePanel.Bitmap, EffectsList);
  end;
  EffectForm.Close; }
  AddEffects(MainForm.InvertEffect1);
end;

procedure TEffectForm.MonohromeItemClick(Sender: TObject);
var i:integer;
begin
 { if FormConfig.SwitchEffect.IsChecked = false then
  begin
    for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
      if MainForm.ImagePanel.Children[i] is TImageFXEffect then
      begin
        MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
        break;
      end;
      MainForm.ImagePanel.AddObject(MainForm.MonochromeEffect1);
  end
  else begin
    EffectsList.Add(MainForm.MonochromeEffect1);
    ApplayEffects(MainForm.ImagePanel.Bitmap, EffectsList);
  end;
  EffectForm.Close;  }
  AddEffects(MainForm.MonochromeEffect1);
end;

procedure TEffectForm.NoiseItemClick(Sender: TObject);
begin
  //NoiseForm.Show;
  //NoiseForm.ImageNoise.Bitmap.Assign(MainForm.ImagePanel.Bitmap);
end;

procedure TEffectForm.SepiaItemClick(Sender: TObject);
var i:integer;
begin
 { if FormConfig.SwitchEffect.IsChecked = false then
  begin
    for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
      if MainForm.ImagePanel.Children[i] is TImageFXEffect then
      begin
        MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
        break;
      end;
      MainForm.ImagePanel.AddObject(MainForm.SepiaEffect1);
  end
  else begin
    EffectsList.Add(MainForm.SepiaEffect1);
    ApplayEffects(MainForm.ImagePanel.Bitmap, EffectsList);
  end;
  EffectForm.Close;   }
  AddEffects(MainForm.SepiaEffect1);
end;

procedure TEffectForm.ShadowItemClick(Sender: TObject);
var i:integer;
begin
  for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
    if MainForm.ImagePanel.Children[i] is TImageFXEffect then
    begin
      MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
      break;
    end;
  MainForm.ImagePanel.AddObject(MainForm.ShadowEffect1);
  EffectForm.Close;
end;

end.
