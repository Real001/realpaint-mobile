program RealPaintM;

uses
  System.StartUpCopy,
  FMX.Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  EffectUnit in 'EffectUnit.pas' {EffectForm},
  NoiseUnit in 'NoiseUnit.pas' {NoiseForm},
  ContrastUnit in 'ContrastUnit.pas' {ContrastForm},
  ConfigUnit in 'ConfigUnit.pas' {FormConfig};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TEffectForm, EffectForm);
  Application.CreateForm(TNoiseForm, NoiseForm);
  Application.CreateForm(TContrastForm, ContrastForm);
  Application.CreateForm(TFormConfig, FormConfig);
  Application.Run;
end.
