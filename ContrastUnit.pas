unit ContrastUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Controls.Presentation, FMX.Filter.Effects, FMX.Effects;

type
  TContrastForm = class(TForm)
    PanelMenu: TPanel;
    PanelImage: TPanel;
    PanelButton: TPanel;
    Label1: TLabel;
    TrackBar: TTrackBar;
    Image1: TImage;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    ContrastEffect1: TContrastEffect;
    procedure TrackBarChange(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ContrastForm: TContrastForm;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

uses
  MainUnit;

procedure TContrastForm.ButtonCancelClick(Sender: TObject);
begin
  ContrastForm.Close;
end;

procedure TContrastForm.ButtonOkClick(Sender: TObject);
var i:integer;
begin
  for i := 0 to MainForm.ImagePanel.ChildrenCount-1 do
    if MainForm.ImagePanel.Children[i] is TImageFXEffect then
    begin
      MainForm.ImagePanel.RemoveObject(MainForm.ImagePanel.Children[i]);
      break;
    end;
    MainForm.ContrastEffect1.Contrast:=TrackBar.Value;
    MainForm.ImagePanel.AddObject(MainForm.ContrastEffect1);
    ContrastForm.Close;
end;

procedure TContrastForm.TrackBarChange(Sender: TObject);
var i:integer;
begin
  Image1.Bitmap.Assign(MainForm.ImagePanel.Bitmap);
  for i := 0 to Image1.ChildrenCount-1 do
    if Image1.Children[i] is TImageFXEffect then
    begin
      Image1.RemoveObject(Image1.Children[i]);
      break;
    end;
    ContrastEffect1.Contrast:=TrackBar.Value;
    Image1.AddObject(ContrastEffect1);
end;

end.
