unit ConfigUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation;

type
  TFormConfig = class(TForm)
    SwitchEffect: TSwitch;
    Label1: TLabel;
    ButtonExit: TButton;
    procedure ButtonExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormConfig: TFormConfig;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

procedure TFormConfig.ButtonExitClick(Sender: TObject);
begin
  FormConfig.Close;
end;

end.
