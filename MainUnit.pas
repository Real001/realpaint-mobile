unit MainUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Objects, System.ImageList, FMX.ImgList,
  FMX.MediaLibrary.Actions, FMX.StdActns, System.Actions, FMX.ActnList, PaintUnit,
  EffectUnit, FMX.Effects, FMX.Filter.Effects, System.Generics.Collections, ConfigUnit;

type
  TMainForm = class(TForm)
    PanelButton: TPanel;
    CameraButton: TButton;
    ActionList: TActionList;
    Action1: TAction;
    TakePhotoFromCameraAction1: TTakePhotoFromCameraAction;
    TakePhotoFromLibraryAction1: TTakePhotoFromLibraryAction;
    ImageList1: TImageList;
    OpenImageButton: TButton;
    PanelImage: TPanel;
    ImagePanel: TImage;
    EffectButton: TButton;
    MonochromeEffect1: TMonochromeEffect;
    SaveButton: TButton;
    InvertEffect1: TInvertEffect;
    SepiaEffect1: TSepiaEffect;
    ContrastEffect1: TContrastEffect;
    ConfigButton: TButton;
    BlurEffect1: TBlurEffect;
    ShadowEffect1: TShadowEffect;
    procedure TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
    procedure TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
    procedure PaintButtonClick(Sender: TObject);
    procedure EffectButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ConfigButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  function ApplayEffects(const Source: TBitmap;
          const List: TObjectList<TImageFXEffect>):TBitmap;

var
  MainForm: TMainForm;
  EffectsList: TObjectList<TImageFXEffect>;

implementation

{$R *.fmx}
{$R *.NmXhdpiPh.fmx ANDROID}

function ApplayEffects(const Source: TBitmap;
          const List: TObjectList<TImageFXEffect>):TBitmap;
var TempForm:TForm;
  TempImage:TImage;
  TempBitmap:TBitmap;
  i:integer;
begin
//��������� ����� � ��������� ������������
  TempForm:=TForm.CreateNew(Application);
  TempForm.ClientWidth:=Source.Width;
  TempForm.ClientHeight:=Source.Height;
  TempImage:=TImage.Create(TempForm);
  TempImage.Parent:=TempForm;
  TempImage.Width:=Source.Width;
  TempImage.Height:=Source.Height;
  TempImage.Bitmap.Assign(Source);
  TempBitmap:=TBitmap.Create(Source.Width,Source.Height);
 // ��������������� ��������� ��� �������
  for i:=0 to List.Count-1 do
  begin
    TempImage.AddObject(List.Items[i]);//��������� ������
    TempBitmap.Canvas.BeginScene();
    TempForm.PaintTo(TempBitmap.Canvas); //�����������
    TempBitmap.Canvas.EndScene;
    TempImage.RemoveObject(List.Items[i]); //������� ������
    TempImage.Bitmap.Assign(TempBitmap);
  end;
  Result:=TempImage.Bitmap;
  TempBitmap.Free;
  TempForm.Release;
end;


procedure TMainForm.SaveButtonClick(Sender: TObject);
var
  TempForm: TForm;
  TempImage: TImage;
  TempBitmap: TBitmap;
begin
  TempForm:=TForm.Create(Application);
  TempForm.ClientWidth:=ImagePanel.Bitmap.Width;
  TempForm.ClientHeight:=ImagePanel.Bitmap.Height;
  TempImage:=TImage.Create(TempForm);
  TempImage.Parent:=TempForm;
  TempImage.Width:=ImagePanel.Bitmap.Width;
  TempImage.Height:=ImagePanel.Bitmap.Height;
  TempImage.Bitmap.Assign(ImagePanel.Bitmap);
  TempImage.AddObject(MonochromeEffect1);
  TempBitmap:=TBitmap.Create(ImagePanel.Bitmap.Width, ImagePanel.Bitmap.Height);
  TempBitmap.Canvas.BeginScene();
  TempForm.PaintTo(TempBitmap.Canvas);
  TempBitmap.Canvas.EndScene;
  TempBitmap.SaveToFile('RealPaint/1.bmp');
  TempBitmap.Free;
  TempForm.Release;
end;

procedure TMainForm.ConfigButtonClick(Sender: TObject);
begin
  FormConfig.Show;
end;

procedure TMainForm.EffectButtonClick(Sender: TObject);
begin
  EffectForm.Show;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  EffectsList:=TObjectList<TImageFXEffect>.Create(false);
end;

procedure TMainForm.PaintButtonClick(Sender: TObject);
begin
  PaintForm.Show;
end;

procedure TMainForm.TakePhotoFromCameraAction1DidFinishTaking(Image: TBitmap);
begin
  ImagePanel.Bitmap.Assign(Image);
end;

procedure TMainForm.TakePhotoFromLibraryAction1DidFinishTaking(Image: TBitmap);
begin
  ImagePanel.Bitmap.Assign(Image);
end;

end.
